window.onload = function(){
    const pattern = /\d\d\d/g;// регулярное выражение с пом литерала
    const pattern1 = RegExp("\d\d\d"); // класс RegExp

    const text = 'Simple red 123 text 434 of the test 256 is cool589';

    text.search(pattern);
    var result = text.search(pattern);// возвращает индекс первого символа на котором нашел совпадение
    //document.write(result); 

    var res2 = text.replace(pattern,"");//замена 
    //document.write(res2);

    var res3 = text.match(pattern);//возвращает совпадения
   // document.write(" "+res3);

    var res4 = text.split(pattern);// делит по паттерну строку
  //  document.write('   '+res4);

    res5 = pattern.exec(text);
   // document.write('   '+res5.index);

    var res6 = pattern.test(text);
   // document.write('   '+res6);

   const phonePattern = /[+]375 [(][\d]{2}[)] \d{3}-\d{2}-\d{2}/;

   var get = function(id){
       return document.getElementById(id);
   }

   get('check').addEventListener('click',testPhoneNumber);

   function testPhoneNumber(){
       const phone = get('phone').value;

       if(phonePattern.test(phone)==true){
           alert('Телефон введен верно');
           get('phone').value = '';
       }else{
           alert('Телефон введен неверно!!!');
       }
   }
}